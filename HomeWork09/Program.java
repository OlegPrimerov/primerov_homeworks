package HomeWork09;

public class Program {
    public static void main(String[] args) {

        Figure figure = new Figure(1, 1);
        Rectangle rectangle = new Rectangle(0, 0, 2, 3);
        Square square = new Square(1, 1, 5);
        Ellipse ellipse = new Ellipse(1, 1, 4, 2);
        Circle circle = new Circle(1, 1, 8);

        Figure[] figures = new Figure[5];
        figures[0] = figure;
        figures[1] = rectangle;
        figures[2] = square;
        figures[3] = ellipse;
        figures[4] = circle;

        for (int i = 0; i < figures.length; i++) {
            double ref = figures[i].getPerimeter();
            System.out.println(ref);
        }
    }
}
