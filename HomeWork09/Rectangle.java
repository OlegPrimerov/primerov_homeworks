package HomeWork09;

public class Rectangle extends Figure {
    private int a;
    private int b;

    public Rectangle(int x, int y, int a, int b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public int getArea() {
        return a * b;
    }

    public double getPerimeter() {
        return a * 2 + b * 2;
    }
}


