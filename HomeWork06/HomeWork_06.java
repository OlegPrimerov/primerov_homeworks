public class HomeWork_06 {

    /**
     * функция возвращающая индекс числа массива
     * @param arr массив в котором необходимо найти индекс числа
     * @param digit число индекс которого необходимо найти
     * @return
     */
    public static int showIndex (int[] arr, int digit ) {
//        проходим по массиву и сравнимаем i-ый элемент с digit
        for (int i = 0; i < arr.length ; i++) {
            if (arr[i]==digit) {
                return i;
            }
        }return -1;
    }

    /**
     * Процедура сдвигающая все значимые числа массива в начало
     * @param array массив в которум нужно сдвинуть все значимые числа массива в начало
     */
    public static void moveZero (int array[]){
//        Количество позиций на которое нужно сдвинуть i-ый элемент в лево
        int j = 0;
//        проходим по массиву и сдвигаем все значимые числа в лево
        for (int i = 0; i <array.length ; i++) {
            if (array[i] == 0)j++;
            else array[i-j] = array[i];
        }
//        проходим по оставшейся части массива и заполняем его нулями
        for (int i = array.length-j; i <array.length ; i++) {
            array[i] = 0;
        }
    }

    /**
     * Процедура выводящая массив на печать
     * @param arr массив который нужно вывести на печать
     */
    public static void printArray (int arr[]) {
        for (int i = 0; i <arr.length ; i++) {
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }

//    public static void main(String[] args) {
//        Проверяем задачу №1
//        int arr1[] = {0,10,20,30,40,50,60,70};
//        System.out.println(showIndex(arr1, 5));
//        System.out.println(showIndex(arr1, 0));
//        System.out.println(showIndex(arr1, 50));

//        Проверяем задачу №2
//        int[] arr2 = {34,0,0,14,15,0,18,0,0,1,20};
//        printArray(arr2);
//        moveZero(arr2);
//        printArray(arr2);
//    }
}
