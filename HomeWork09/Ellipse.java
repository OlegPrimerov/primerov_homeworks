package HomeWork09;

public class Ellipse extends Figure {
    protected double radius1;
    private double radius2;

    public Ellipse(int x, int y, double radius1, double radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public void paint() {
        System.out.println("Овал - " + radius1 + " " + radius2);
    }

    public double getPerimeter() {
        return (4 * ((((radius1 - radius2) * (radius1 - radius2)) + (3.14 * radius1 * radius2)) / (radius1 + radius2)));
    }
}



