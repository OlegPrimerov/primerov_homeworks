import java.util.Scanner;

public class HomeWork_07 {
    public static void main(String[] args) {
//        Вводим вспомогательные переменные
        int min = 2147483647;
        int num = 0;
//        Создаем массив для хранения повторяющихся чисел
        int array[] = new int[201];
//        Создаем сканнер для ввода чисел
        Scanner scanner = new Scanner(System.in);
//        Задаем ограничиввающие условия для ввода чисел
        while (num != -1) {
            num = scanner.nextInt();
            if (num < -100 || num > 100) {
                System.out.println("Введите число в диапазоне от -100 до 100");
            } else if (num == -1) {
                break;
            } else {
                array[num + 100]++;
            }
        }
//        Алгоритм для нахождения числа повторяющегося минимальное количество раз
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                if (array[i] < min){
                    min = array[i];
                    num = i-100;
                }
            }
        }
        System.out.println(num);
    }
}
