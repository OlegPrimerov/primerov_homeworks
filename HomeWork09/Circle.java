package HomeWork09;

public class Circle extends Ellipse {
    public Circle(int x, int y, double radius) {
        super(x, y, radius, radius);
    }

    public void paint() {
        System.out.println("Круг с радиусом - " + radius1);
    }

    public double getPerimeter() {
        return (6.28 * radius1);
    }
}
