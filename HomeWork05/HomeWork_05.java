import java.util.Scanner;

public class HomeWork_05 {

    public static void main(String[] args) {
//        Создаем переменную для хранения числа
        int num = 9;
//        Создаем переменную для хранения последней цифры
        int dig;
//        Создаем переменную для хранения минимальной цифры
        int min = 9;
//        Создаем сканер для ввода числа
        Scanner scanner = new Scanner(System.in);

//        создаем основной цикл алгоритма поиска минимальной цифры числа
        while (num != -1) {
//            вводим число в консоль и записываем его в переменную mun
            num = scanner.nextInt();
            if (num > -1) {
//                создаем цикл перебора цифр числа и нахождения минимальной цифры этого числа
                while (num != 0) {
                    dig = num % 10;
                    if (dig < min) min = dig;
                    num = num / 10;
                }
            } else if (num < -1) System.out.println("Введите положительное число");
            else System.out.println("Минимальная цифра = " + min);
        }
    }
}
